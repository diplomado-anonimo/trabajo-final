#!/usr/bin/env python
import sys
import os
import json
import datetime
import shutil
from subprocess import Popen, PIPE

# La idea del script es dar una lista de usuarios y descargar sus tuits desde cierta fecha, guardando un archivo por usuario, con todos los tuits dentro


# Cuentas de las que queremos bajar los tuits
seguidos = "mujerescreando1,YerkoGarafulic,asaoyvino,TataQuispe,roblaser,carlosdmesag,RicardoAglrA,ricardopazb,ccorderc,LuisFerCamachoV,roxAhoracRoxana,OscarOrtizA,Csanchezberzain,JeanineAnez,ArturoMurilloS,Ferlopezjulio1,LuchoXBolivia,EvaCopa_Bol,LeonardoLoza18,evoespueblo,canela_cs,SoyUnCocodriloS,gabrielaSCZ,grupoeldeber,LaRazon_Bolivia,unitelbolivia,pagina_siete,ErbolDigital,LosTiemposBol,el_pais,DVerdadBolivia,KawsachunCoca,CFValverde,JohnArandia,raulpenaranda1,meryvaca,MariaSTrigo,AndrsGomezV,TinchoSoyOk,LuciaIreneTrepp,eldelagora,JhanisseVDaza,ErickForonda2"
#seguidos = "JhanisseVDaza, ErickForonda2"
fecha = str(datetime.date.today())
resultado = "resultados/perfiles-"+fecha+'.json'

#for x in cuentas2:
#  script = 'twint -u ' + x +' --since '+ fecha +' -o tuits'+x + fecha+'.json --json'
#  os.system(script)
script = 'twarc users ' + seguidos +' > '+resultado
tuiteros = Popen(script,shell=True,stdout=PIPE,stderr=PIPE)
stdout, stderr = tuiteros.communicate()
lineas = open(resultado,"r+")
archivo_final = open('resultados/los-'+resultado,"a")
archivo_final.write("[\n")
# loop para que agregue una coma antes del salto de línea
for linea in lineas:
    linea = linea.replace("\n",",\n")
    archivo_final.write(linea)
archivo_final.close()

documento = open('los-'+resultado, "r").readlines()
documento_final = open('final-'+resultado, "a")
new_last_line = (documento[-1].replace(",\n", "\n]"))
documento[-1] = new_last_line

for linea in documento:
    documento_final.write(linea)
documento_final.close()

# borro los archivos temporales que fui creando.
# falta moverlo al directorio final o procesarlo en la base de datos

os.remove(resultado)
os.remove('resultados/los-'+resultado)

