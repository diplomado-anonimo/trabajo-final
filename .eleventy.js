module.exports = function (eleventyConfig) {
    // Folders to copy to output folder
    eleventyConfig.addPassthroughCopy("11ty/assets/css");
    eleventyConfig.addPassthroughCopy("11ty/assets/js");
    eleventyConfig.addPassthroughCopy("11ty/assets/font");
    eleventyConfig.addPassthroughCopy("11ty/assets/imgs");
    eleventyConfig.addShortcode("linkecito", function(usuario){
      return "/trabajo-final/"+usuario+"/index.html"
      });
    return {
      markdownTemplateEngine: 'njk',
      dataTemplateEngine: 'njk',
      htmlTemplateEngine: 'njk',
  };
  };