module.exports = function (eleventyConfig) {
    // Folders to copy to output folder
    eleventyConfig.addPassthroughCopy("assets/css");
    eleventyConfig.addPassthroughCopy("assets/js");
    eleventyConfig.addPassthroughCopy("assets/font");
    eleventyConfig.addPassthroughCopy("assets/imgs");
    eleventyConfig.addShortcode("linkecito", function(usuario){
      return "/"+usuario+"/index.html"
      });
    eleventyConfig.addShortcode("textos", function(usuario){
      antes = usuario;
      return antes
    });
    return {
      markdownTemplateEngine: 'njk',
      dataTemplateEngine: 'njk',
      htmlTemplateEngine: 'njk',
  };
  };