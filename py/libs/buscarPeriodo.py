import MySQLdb
from libs.db import database
from libs.wordcloud import buildWordCloud
import os

def buscaPeriodo(nombre,sql,folder=""):
    if(folder==""):
        folder="../resultados/wordcloud/"+nombre
    else:
        folder="../resultados/wordcloud/"+folder
        
    folder+="/"
   
    palabras=database(sql,1)
    
    
    try:
        os.makedirs(folder)
    except FileExistsError as exc:
        print("ya existe el directorio ",folder)

    buildWordCloud(nombre,palabras,folder)
