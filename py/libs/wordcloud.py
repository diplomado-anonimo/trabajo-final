from wordcloud import WordCloud, ImageColorGenerator
import nltk
import matplotlib.pyplot as plt
import re
from PIL import Image
import numpy as np
nltk.download("popular")
from nltk.corpus import stopwords


def buildWordCloud(name,palabras,folder):
    #sacamos wordcloud       
    
    
    pattern=r'(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))';

    match = re.findall(pattern, palabras)
    stopwords_propias = ['…','.',' De ',' de ',' la ',' el ',' del ',' en ',' En ',' qué ',' mi ',' Mi ',' El ','última','Nacional','último','Bolivia','VideoNoticias','P7Informa Bolivia','P7Informa','Ver más', 'twitter','https','twitter.com',' Me ',' La ','Ver nota']
    for m in match:
        url = m[0]
        palabras = palabras.replace(url, ' ')

    palabras=palabras.strip()
    
    for m in stopwords_propias:
        palabras = palabras.replace(m, ' ')
    
    
    if(palabras.strip()==""):
        palabras = "No tweets"

    stopwords_esp = stopwords.words('spanish') 
    
    stopwords_en = stopwords.words('english') 

    # Puedes adicionar a la lista las palabras propias del contexto de la noticia.. no relevantes que 
    # no deberían mostrarse en el wordcloud

    stopwords_esp.extend(stopwords_propias)
    stopwords_esp.extend(stopwords_en)
    
    palabras = [word for word in palabras.split() if word not in stopwords_esp]
    
    palabras= " ".join(palabras)
    
    # Volvamos a crear el wordcloud, pero esta vez, vamos a remover las stopwords
    wordcloud = WordCloud(max_words=150, background_color="white", stopwords=stopwords_esp).generate(palabras)
    plt.figure(1, figsize=(10, 10))
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.title(name)
    plt.savefig(folder+name+'.png')

