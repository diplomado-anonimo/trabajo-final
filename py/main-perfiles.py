#!/usr/bin/python

import sys
import os


sys.path.append("libs")

from libs.db import database
from libs.wordcloud import buildWordCloud
from libs.buscarPeriodo import buscaPeriodo


if len(sys.argv) >1 :
    print(sys.argv[1])   # pondremos aqui los parámetros para ejecutarlos
    

sql = "select * from periodos where activo=1"
periodos=database(sql,0,2)

sql = "select id,screen_name from profile order by screen_name"
profiles=database(sql,0,2)  #llamamos a todos los perfiles
for perfil in profiles :
    for fechas in periodos : 
        
        print(fechas[3],"--",fechas[4])
        sql = 'select GROUP_CONCAT(tweet.tweet SEPARATOR " " ) from tweet inner join profile on profile.id=tweet.user_id \
            where tweet.date between "{}" and "{}" \
                and profile.id="{}"'
        sql=sql.format(fechas[3],fechas[4],perfil[0])
        buscaPeriodo(fechas[1]+"-"+str(fechas[3])+"-"+str(fechas[4]),sql,'personal/'+perfil[1])
