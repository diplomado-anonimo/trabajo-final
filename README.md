# Trabajo final para el diplomado

## Librerías

Estaremos usando principalmente dos librerías para la descarga de tweets:

* [twarc](https://github.com/DocNow/twarc) - Para usar esta librería es necesario contar con una cuenta para desarrolladores. En principio la usamos para capturar los perfiles de las cuentas de twitter que nos interesan.

* [twint](https://github.com/twintproject/twint/) - No es necesario tener una cuenta de desarrollador y permite descargar tuits históricos.

## Tareas

* [x] Descargar los perfiles de las cuentas que nos interesan. Estos perfiles brindan mucha información del usuario. `twarc users *nombreDeUsuario*`
* [x] Descargar los tweets históricos de los usuarios que nos interesan desde el día previo de las elecciones del 20 de octubre, hasta ahora
* [ ] Crear *programa* que descargue los nuevos tweets periódicamente
* [ ] Crear sitio web donde puedan verse los resultados

## Comandos que corrimos para conseguir los tuits

Para recuperar los tuits desde octubre de 2019 utilizamos la librería twint con el siguiente comando.

`twint -u usuarioQueQueremosDescargar --since fecha -o tuits'+ nombreDeUsuario + fecha+'.json --json`

Para recuperar los perfiles de todas las entidades que seguimos usamos el siguiente comando de *twarc*.

`twarc users lista_de_usuarios > perfiles.jsonl`

Este archivo es una lista de *jsones* y hay que editarla un poco para que pueda ser leída en 11ty.

## Sitio web

Para arar el sitio web estamos usando [11ty](https://www.11ty.dev) para generar el sitio estático y [tailwindcss](https://tailwindcss.com/) como framework de CSS. Ambos son librerías de Node. Hay que instalarlas dentro del directorio 11ty del repositorio con:

* `npm install @11ty/eleventy --save-dev`
* `npm install tailwindcss --save-dev`

## Resultados

* Las nubes de palabras se consiguen mediante un script que hace la llamada a la base de datos a partir de ciertos períodos. Una vez creadas las imágenes, como son muy grandes, las recortamos con estos scripts ```mkdir finales && mogrify -crop 820x500+100+250 -path finales/ *.png ```