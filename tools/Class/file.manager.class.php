dir<?php

/**
 * Function que nos da los elementos de una carpeta
 *
 * @author norman
 */
class fileManager {

    /**
     *
     * @var string nombre de la carpeta 
     */
    public $dir = null;
    private $handle = null;
    private $conexion = false;

    function __construct($dir) {
        if (trim($dir) == '') {
            die('no se especifico el directorio');
        }
        $this->setDir($dir);
        $this->conexion = $this->handle = opendir($dir);
    }

    /**
     * Cerramos el conector a IO
     */
    function __destruct() {
        if ($this->handle) {
            closedir($this->handle);
        }
    }

    /**
     * Asignación de Directorio para buscar
     * @param type $dir
     */
    function setDir($dir) {
        $this->dir = $dir;
    }

    /**
     * Funcion que nos pide los elementos 
     * @param Bool $onlyFiles  si nos muestra directorios más @todo
     */
    function getDirElements($onlyFiles = true) {
        /* This is the correct way to loop over the directory. */
        if (false !== ($entry = readdir($this->handle))) {
            return $entry;
        }
        return false;
    }

    function getConexion() {
        return $this->conexion;
    }

}
