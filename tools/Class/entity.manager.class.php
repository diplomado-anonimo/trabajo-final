<?php

class entityManager {

    private $dbconn = null;
    private $user = null;
    private $pdo = null;

    /**
     * constructor que recibe los parametros por defecto 
     */
    function __construct($host = 'localhost', $db = 'tw', $user = 'root', $pass = 'normmy2k', $port = '3306', $charset = 'utf8mb4') {
        $pdo = null;

        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset;port=$port";
        try {
            $pdo = new \PDO($dsn, $user, $pass, $options);
            $this->setPdo($pdo);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int) $e->getCode());
        }
        $this->setPdo($pdo);
    }

    public function setPdo($pdo) {
        $this->pdo = $pdo;
    }

    public function getPdo($pdo) {
        return $this->pdo;
    }

    public function insert($table, $fields, $data) {
        $fieldsArray = array_map('trim', explode(',', $fields));
        $newDat = [];
        foreach ($fieldsArray as $value) {
            if (array_key_exists($value, $data)) {
                $newDat[$value] = $data[$value];
            } else {
                $newDat[$value] = "";
            }
        }
        $data = $newDat;

        $_values = ":" . implode(', :', $fieldsArray);
        $data = array_map('convertJSON', $data);
        
        try {
            $sql = "
                    INSERT INTO $table
                        ($fields)
                    VALUES
                        ($_values); ";
            
            $sqlprep = $this->pdo->prepare($sql);
            $sqlprep->execute($data);
        } catch (PDOException $e) {

            if ($e->errorInfo[0] == 23000) {
                echo $data['name'] . " ya se encuentra en la base de datos";
            } else {
                echo $data['name'] . " tiene error al ingresar a la base de datos<br>";
                echo $e->getMessage();
            }
            echo "</br><hr>";
        }
    }

    public function getProfileFields() {
        return 'id, id_str, name, screen_name, location, description, '
                . 'url, entities, protected, followers_count, friends_count, listed_count, created_at, favourites_count, '
                . 'utc_offset, time_zone, geo_enabled, verified, statuses_count, lang, status, '
                . 'contributors_enabled, is_translator, is_translation_enabled, profile_background_color, '
                . 'profile_background_image_url, profile_background_image_url_https, profile_background_tile, '
                . 'profile_image_url, profile_image_url_https, profile_banner_url, profile_link_color, '
                . 'profile_sidebar_border_color, profile_sidebar_fill_color, profile_text_color, '
                . 'profile_use_background_image, has_extended_profile, '
                . 'default_profile, default_profile_image, following, follow_request_sent, notifications, translator_type';
    }

    public function getTwitFields() {

        return 'id, conversation_id, created_at, date, time, timezone, user_id, username, name, '
                . ' place, tweet, mentions, urls, photos, replies_count, retweets_count, likes_count, '
                . 'hashtags, cashtags, link, retweet, quote_url, video, near, geo, source, '
                . 'user_rt_id, user_rt, retweet_id, reply_to, retweet_date, translate, trans_src, trans_dest';
    }

    public function getArchivosFields(){
        return 'id, path, fecha';
        
    }
}
