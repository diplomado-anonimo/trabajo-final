<?php
header( 'Content-type: text/html; charset=utf-8' );
include "Class/entity.manager.class.php";
include "Class/file.manager.class.php";
include "Class/aux.php";
/**
 * Archivo que recupera todos los archivos JSON y una vez asignados los mueve a carpeta procesados
 * 
 */
$folder = "../tuits_hasta_ahora/para_procesar";
$em = new entityManager();
$debug = false;
$manfol = new fileManager($folder);
$f = 0;
/**
 * Look for each file in tuits
 */
if ($manfol->getConexion()) { //si tenemos conexión
    while (false !== ($entry = $manfol->getDirElements())) {
        $f=0;
        if (strlen($entry)>3 && $entry!='procesados') {
            echo "<hr/>", $folder . "/" . $entry, "<hr/>";

            $myfile = fopen($folder . "/" . $entry, "r") or die("Unable to open file!");
            // Output one line until end-of-file
            while (!feof($myfile)) {
                $f++;
                $JSON = json_decode(fgets($myfile));
                $em->insert('tweet', $em->getTwitFields(), (array)$JSON);
            }
            echo $f;
            fclose($myfile);
            echo $folder . "/../procesados/" . $entry;
            $val = rename($folder . "/" . $entry, $folder . "/../procesados/" . $entry);
            if($val===false){
                echo "no se pudo mover los archivos procesados, verifique que procesados y por procesaar tengan permiso de escritura";
            }
        }
    }
}
