-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: tw
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `archivos`
--

DROP TABLE IF EXISTS `archivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `NewTable_path_IDX` (`path`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_str` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entities` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`entities`)),
  `protected` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `followers_count` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friends_count` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `listed_count` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favourites_count` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `utc_offset` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_zone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geo_enabled` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statuses_count` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contributors_enabled` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_translator` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_translation_enabled` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_background_color` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_background_image_url` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_background_image_url_https` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_background_tile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image_url` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image_url_https` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_banner_url` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_link_color` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_sidebar_border_color` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_sidebar_fill_color` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_text_color` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_use_background_image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_extended_profile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_profile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_profile_image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `following` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `follow_request_sent` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notifications` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `translator_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Profile table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tweet`
--

DROP TABLE IF EXISTS `tweet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tweet` (
  `id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversation_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `timezone` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `username` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tweet` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mentions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photos` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `replies_count` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `retweets_count` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likes_count` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hashtags` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cashtags` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `retweet` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quote_url` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `near` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_rt_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_rt` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `retweet_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_to` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `retweet_date` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `translate` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_src` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_dest` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Message tables';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'tw'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-07 15:13:41
